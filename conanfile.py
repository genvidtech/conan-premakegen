from conans.model import Generator
from conans.paths import BUILD_INFO
from conans import ConanFile, CMake

class PremakeDeps(object):
    def __init__(self, deps_cpp_info):
        self.include_paths = ",\n".join('"%s"' % p.replace("\\", "/")
                                       for p in deps_cpp_info.include_paths)
        self.lib_paths = ",\n".join('"%s"' % p.replace("\\", "/")
                                   for p in deps_cpp_info.lib_paths)
        self.bin_paths = ",\n".join('"%s"' % p.replace("\\", "/")
                                   for p in deps_cpp_info.bin_paths)
        self.libs = ", ".join('"%s"' % p for p in deps_cpp_info.libs)
        self.defines = ", ".join('"%s"' % p for p in deps_cpp_info.defines)
        self.cppflags = ", ".join('"%s"' % p for p in deps_cpp_info.cppflags)
        self.cflags = ", ".join('"%s"' % p for p in deps_cpp_info.cflags)
        self.sharedlinkflags = ", ".join('"%s"' % p for p in deps_cpp_info.sharedlinkflags)
        self.exelinkflags = ", ".join('"%s"' % p for p in deps_cpp_info.exelinkflags)

        self.rootpath = "%s" % deps_cpp_info.rootpath.replace("\\", "/")
        
class Premake(Generator):
    @property
    def filename(self):
        return "conanpremake.lua"

    @property
    def content(self):     
        deps = PremakeDeps(self.deps_build_info)

        template = ('conan.{dep} = {{}}\n'
                    'conan.{dep}.includedirs = {{{deps.include_paths}}}\n'
                    'conan.{dep}.libdirs = {{{deps.lib_paths}}}\n'
                    'conan.{dep}.bindirs = {{{deps.bin_paths}}}\n'
                    'conan.{dep}.libs = {{{deps.libs}}}\n'
                    'conan.{dep}.cppdefines = {{{deps.defines}}}\n'
                    'conan.{dep}.cppflags = {{{deps.cppflags}}}\n'
                    'conan.{dep}.cflags = {{{deps.cflags}}}\n'
                    'conan.{dep}.sharedlinkflags = {{{deps.sharedlinkflags}}}\n'
                    'conan.{dep}.exelinkflags = {{{deps.exelinkflags}}}\n')

        sections = ["#!lua", "local conan = {}"]
        all_flags = template.format(dep="ROOT", deps=deps)
        sections.append(all_flags)
        template_deps = template + 'conan.{dep}.rootpath = "{deps.rootpath}"\n'

        # Dump one section per package.
        for dep_name, dep_cpp_info in self.deps_build_info.dependencies:
            deps = PremakeDeps(dep_cpp_info)
            dep_name = dep_name.replace("-", "_")
            dep_flags = template_deps.format(dep=dep_name, deps=deps)
            sections.append(dep_flags)

        # Dump utility methods, e.g. use().
        methods = """
-- A utility routine to efficiently add packages to a Premake project.
-- It handles the defines(), includedirs(), libdirs(), and links() calls.
-- E.g. conan.use('myLib', 'myOtherLib')
conan.use = function(...)
    local args = {...} -- LuaJIT compatibility.
    for i,v in ipairs(args) do
        local dep_name = v:gsub("-", "_")
        local pkg_vars = conan[dep_name]
        if pkg_vars then
            defines{ pkg_vars.cppdefines }
            includedirs{ pkg_vars.includedirs }
            libdirs{ pkg_vars.libdirs }
            links{ pkg_vars.libs }
        else
            error("Conan package not found: '"..v.."'.")
        end
    end
end
"""[1:]
        sections.append(methods)

        sections.append("return conan\n")

        return "\n".join(sections)


class MyCustomGeneratorPackage(ConanFile):
    name = "PremakeGen"
    version = "0.3"
    url = "https://bitbucket.org/genvidtech/conan-premake"
    license = "MIT"
    description = "A premake generator for conan."
    
    def build(self):
      pass
    
    def package_info(self):
      self.cpp_info.includedirs = []
      self.cpp_info.libdirs = []
      self.cpp_info.bindirs = []
