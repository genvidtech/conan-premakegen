#!lua

conan = include 'conanpremake'
root = conan.ROOT

-- A solution contains projects, and defines the available configurations
solution "MyApplication"
   configurations { "Debug", "Release" }
   includedirs { root.includedirs }
   libdirs { root.libdirs }
   links { root.libs }
   -- A project defines one build target
   project "MyApplication"
      kind "ConsoleApp"
      language "C++"
      files { "**.h", "**.cpp" }
 

      configuration "Debug"
         defines { "DEBUG" }
         symbols "On"

      configuration "Release"
         defines { "NDEBUG" }
         optimize "On"
