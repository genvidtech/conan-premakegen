from conans import ConanFile, RunEnvironment, tools
import os
import shutil

channel = os.getenv("CONAN_CHANNEL", "1.4.0")
username = os.getenv("CONAN_USERNAME", "genvidtech")


class ConanpremakegenTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "premake/5.0.0-alpha11@genvidtech/1.4.0", \
               ("PremakeGen/0.3@%s/%s" % (username, channel)), \
               # "hello/1.0@bincrafters/testing" # Add this to compile main.cpp.
    generators = "Premake"
    exports = "premake5.lua", "main.cpp"

    def build(self):
        for src in self.exports:
            shutil.copy2(os.path.join(self.source_folder, src), src)
        env_build = RunEnvironment(self)
        with tools.environment_append(env_build.vars):
            self.run("premake5 gmake")

    def test(self):
        pass
